# Galaxy Simulation Presentation

This repository contains the presentation I made for defending my Mphil thesis on the topic **Studies on Orientation Effects of Disk Galaxies: A
Monte Carlo Approach**  under the guidance of [Prof. Titus K Mathew](https://scholar.google.com/citations?hl=en&user=LcoF4IgAAAAJ) and co-guided by [Dr. C. D. Ravikumar](https://scholar.google.com/citations?user=bcWGogUAAAAJ&hl=en&oi=sra).

### License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
